from convertbng.util import convert_lonlat
import numpy as np
from numpy import genfromtxt
air_quality_index = "NO2"
my_data = genfromtxt('CSV/2013_Met2013_LAEI2013_{}.csv'.format(air_quality_index), delimiter=',')
print(len(my_data))
eastings = my_data[:, 0]
print(1)
northings = my_data[:, 1]
print(2)
eastings_np = np.array(eastings)
print(3)
northings_np = np.array(northings)
print(4)
number_of_sample = len(eastings)
value_np = np.array(my_data[:, 2]).reshape(number_of_sample, 1)
print(5)
lonlat_result = convert_lonlat(eastings_np, northings_np)
print(6)
lon_result_np = np.array(lonlat_result[0]).reshape(number_of_sample, 1)
print(7)
lat_result_np = np.array(lonlat_result[1]).reshape(number_of_sample, 1)
print(8)
result = np.concatenate((lat_result_np, lon_result_np, value_np), axis=1)
print(9)
np.savetxt("CSV/converted/converted_result3_{}.csv".format(air_quality_index), result, delimiter=',')
print(10)