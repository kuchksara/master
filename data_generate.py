import random
import requests
import json
import numpy as np

from numpy import genfromtxt
air_quality_index = "NO2"
data = genfromtxt('CSV/converted/converted_result_{}.csv'.format(air_quality_index), delimiter=',')
all_data = data.shape[0]
osm_api_template = "https://routing.openstreetmap.de/routed-car/route/v1/driving/{},{};{},{}?overview=false&geometries=polyline&steps=true"
for i in range(1000):
    print("===============iteration {} =================".format(i))
    key_points = []
    person_const = 5
    index1 = random.randrange(all_data)
    index2 = random.randrange(all_data)
    p1 = data[index1, :]
    p2 = data[index2, :]
    result = requests.get(osm_api_template.format(p1[1], p1[0], p2[1], p2[0]))
    # result = requests.get(osm_api_template.format(p1[0], p1[1], p2[0], p2[1]))
    # print(result.content.decode('utf-8'))
    # print(result.content.decode("utf-8")["routes"])
    # print(result.content.decode("utf-8")["routes"][0])
    # print(result.content.decode("utf-8")["routes"][0]["legs"])
    # print(result.content.decode("utf-8")["routes"][0]["legs"][0]["steps"][0]["intersections"][0]["location"])
    # print(result.content.decode("utf-8")["routes"][0]["legs"][0]["steps"][0]["intersections"][0]["location"])
    # print(result.content.decode("utf-8")["routes"][0]["legs"][0]["steps"][0]["intersections"][0]["location"])
    osm_points = json.loads(result.content.decode("utf-8"))["routes"][0]["legs"][0]["steps"]
    for point in osm_points:
        key_points.append(point["intersections"][0]["location"])
    np.savetxt('CSV/keypoints/{}.csv'.format(i), np.array(key_points), delimiter=',')
